package kea.dk.isobar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan(basePackages = {"kea.dk.isobar","kea.dk.isobar.Repository"})
@SpringBootApplication
public class IsobarApplication {

    public static void main(String[] args) {
        SpringApplication.run(IsobarApplication.class, args);
        //Usikker på nedenstående, tror det er noget chance brugte for at få noget sout ud
        //Når applikationen køres
      //  ApplicationContext context = SpringApplication.run(IsobarApplication.class, args);
    }
}
