package kea.dk.isobar.Controller;

import kea.dk.isobar.Model.Icecream;
import kea.dk.isobar.Model.Ingredient;
import kea.dk.isobar.Repository.IcecreamRepositoryInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.jws.WebParam;

@Controller
    public class IceCreamController {

    @Autowired
    private IcecreamRepositoryInterface icecreamRepositoryInterface;

    @RequestMapping("/icecreams")
    public String is(Model model) {
        model.addAttribute("icecreams_data", icecreamRepositoryInterface.getIceCreams());
        return "icecreams";
    }


    @GetMapping("/createIcecream")
    public String createIcecream() { return "createIcecream";
    }

    @PostMapping("/createIcecream")
    public String createIcecream(@ModelAttribute("name") String icecream) {
        icecreamRepositoryInterface.create(new Icecream(icecream));
        return "redirect:/icecreams";

    }


    @GetMapping("/EditIcecream")
    public String Edit(@RequestParam("id") int id, Model model){
        model.addAttribute("icecreams", icecreamRepositoryInterface.read(id));
        return "EditIcecream";
    }


    @PostMapping("/EditIcecream")
    public String Edit(@ModelAttribute Icecream icecreams, Model model, BindingResult result) {
        if (result.hasErrors())
            System.out.println(result.toString());
        icecreamRepositoryInterface.Edit(icecreams);
        return "redirect:/icecreams";
    }


    @GetMapping("/deleteIcecream")
    public String delete(@RequestParam("id") int id, Model model){
        Icecream icecrceam = icecreamRepositoryInterface.read(id);
        model.addAttribute("icecreams", icecrceam);
        return "deleteIcecream";
    }


    // man kan både bruge post og @DeleteMapping.
    // @DeleteMapping er en sammensat annotation, der fungerer som en genvej til
    // @RequestMapping (metode = RequestMethod.DELETE).
    @PostMapping("/deleteIcecream")
    public String delete(@ModelAttribute("id") int id ){
        icecreamRepositoryInterface.delete(id);
        return "redirect:/icecreams";
    }

}




