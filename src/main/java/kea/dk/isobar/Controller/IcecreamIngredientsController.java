package kea.dk.isobar.Controller;


import kea.dk.isobar.Model.Icecream;
import kea.dk.isobar.Model.IcecreamIngredients;
import kea.dk.isobar.Repository.IcecreamIngredientsRepository;
import kea.dk.isobar.Repository.IcecreamIngredientsRepositoryInterface;
import kea.dk.isobar.Repository.IcecreamRepositoryInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;

@Controller
public class IcecreamIngredientsController {

    @Autowired
    IcecreamIngredientsRepositoryInterface icecreamIngredientsRepository;

    @Autowired
    IcecreamRepositoryInterface icecreamRepository;


    @GetMapping("/ProduceIcecream")
    public String Produce(@RequestParam("id") int id, Model model){
        model.addAttribute("iceCream", icecreamRepository.read(id));
        return "ProduceIcecream";
    }


    @PostMapping("/ProduceIcecream")
    public String Produce(@ModelAttribute Icecream icecream, Model model) {
        ArrayList<IcecreamIngredients> ingredients =
                icecreamIngredientsRepository.getIcecreamIngredients(icecream);

        try {
            icecreamIngredientsRepository.produce(ingredients);
        }catch (Exception ex){
            return "redirect:/error";
        }
        return "redirect:/icecreams";
    }


}
