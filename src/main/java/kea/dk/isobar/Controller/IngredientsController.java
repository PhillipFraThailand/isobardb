package kea.dk.isobar.Controller;

import kea.dk.isobar.Model.Ingredient;
import kea.dk.isobar.Repository.IngredientRepositoryInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;

@Controller
public class IngredientsController {

    @Autowired
    private IngredientRepositoryInterface ingredientsRepository;

    @GetMapping("/")
    public String index() {
        return "index";
    }


    @GetMapping("/ingredients")
    public String ingredients(Model model) {
        model.addAttribute("ingredients_data", ingredientsRepository.getIngredients());
        return "ingredients";
    }

    //Add new ingredient
    @GetMapping ("/addIngredient")
    public  String addIngredient (){
        return "addIngredient";
    }

    @PostMapping ("/addIngredient")

    public String addIngredient (@ModelAttribute("name") String name, @ModelAttribute("inventory") String invetory) throws SQLException {
        ingredientsRepository.addNewIngredient(new Ingredient(1, name, Double.parseDouble(invetory)));
        return "redirect:/ingredients";

    }

    @GetMapping("/Edit")
    public String Edit(@RequestParam("id") int id, Model model){
        model.addAttribute("ingredients", ingredientsRepository.read(id));
        return "Edit";
    }

    // man kan både bruge post og @DeleteMapping.
    // @DeleteMapping er en sammensat annotation, der fungerer som en genvej til @RequestMapping (metode = RequestMethod.DELETE).

    @PostMapping("/Edit")
    public String Edit(@ModelAttribute Ingredient ingredients, Model model, BindingResult result) {
        if (result.hasErrors())
            System.out.println(result.toString());
        ingredientsRepository.Edit(ingredients);
        return "redirect:/ingredients";
    }


    @GetMapping("/delete")
    public String delete(@RequestParam("id") int id, Model model){
        Ingredient ingredi = ingredientsRepository.read(id);
        model.addAttribute("ingredients", ingredi);
        return "delete";
    }


    // man kan både bruge post og @DeleteMapping.
    // @DeleteMapping er en sammensat annotation, der fungerer som en genvej til @RequestMapping (metode = RequestMethod.DELETE).

    @PostMapping("/delete")
    public String delete(@ModelAttribute("id") int id ){
        ingredientsRepository.delete(id);
        return "redirect:/ingredients";
    }

}