package kea.dk.isobar.Model;

public class IcecreamIngredients {


        private int idicecreamingredients;
        private int fkIcecream;
        private int fkIngredients;
        private double Quantity;

        public IcecreamIngredients() {
        }

        public IcecreamIngredients(int idicecreamingredients, int fkIcecream, int fkIngredients, double quantity) {
            this.idicecreamingredients = idicecreamingredients;
            this.fkIcecream = fkIcecream;
            this.fkIngredients = fkIngredients;
            Quantity = quantity;
        }

        public int getIdicecreamingredients() {
            return idicecreamingredients;
        }

        public void setIdicecreamingredients(int idicecreamingredients) {
            this.idicecreamingredients = idicecreamingredients;
        }

        public int getFkIcecream() {
            return fkIcecream;
        }

        public void setFkIcecream(int fkIcecream) {
            this.fkIcecream = fkIcecream;
        }

        public int getFkIngredients() {
            return fkIngredients;
        }

        public void setFkIngredients(int fkIngredients) {
            this.fkIngredients = fkIngredients;
        }

        public double getQuantity() {
            return Quantity;
        }

        public void setQuantity(double quantity) {
            Quantity = quantity;
        }

        @Override
        public String toString() {
            return "IcecreamIngredients{" +
                    "idicecreamingredients=" + idicecreamingredients +
                    ", fkIcecream=" + fkIcecream +
                    ", fkIngredients=" + fkIngredients +
                    ", Quantity=" + Quantity +
                    '}';
        }
    }


