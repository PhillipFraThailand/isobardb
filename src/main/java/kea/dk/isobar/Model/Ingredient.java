package kea.dk.isobar.Model;

public class Ingredient {
    private int id;
    private String name;
    private double inventory;

    public Ingredient() {
    }

    public Ingredient(int idIngredients, String name, double inventory) {
        this.id=idIngredients;
        this.name=name;
        this.inventory=inventory;
    }

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public double getInventory() {
        return inventory;
    }

    public void setInventory(double inventory) {
        this.inventory = inventory;
    }

    @Override
    public String toString() {
        return "Ingredient{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", inventory=" + inventory +
                '}';
    }
}




