package kea.dk.isobar.Model;

import java.util.List;

    public class Icecream {
        private int id;
        private String name;
        private List<Ingredient> ingredients;

        public Icecream() {
        }

        public Icecream(int idIcecreams, String name) {
        this.id=idIcecreams;
        this.name=name;
        }

        public Icecream(String name) {
            this.name = name;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public List<Ingredient> getIngredients() {
            return ingredients;
        }

        public void setIngredients(List<Ingredient> ingredients) {
            this.ingredients = ingredients;
        }
    }


