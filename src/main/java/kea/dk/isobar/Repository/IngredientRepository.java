package kea.dk.isobar.Repository;

import kea.dk.isobar.Model.Ingredient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;

@Repository
public class IngredientRepository implements IngredientRepositoryInterface {

    @Autowired
    JdbcTemplate jdbc;
    SqlRowSet sqlRowSet;

    @Override
    public List<Ingredient> getIngredients() {

        List<Ingredient> ingredients = new ArrayList<>();
        String sql = "SELECT * FROM ingredients";
        sqlRowSet = jdbc.queryForRowSet(sql);


        while (sqlRowSet.next()) {
            ingredients.add(new Ingredient(sqlRowSet.getInt("idIngredients"),
                    sqlRowSet.getString("name"),
                    sqlRowSet.getDouble("inventory")
            ));

        }

        return ingredients;
    }

    @Override
    public List<Ingredient> getIceCreamIngredients(int iceCreamId) {
        List<Ingredient> Ingredientid = new ArrayList<>();
        String sql = "SELECT * FROM ingredients";
        sqlRowSet = jdbc.queryForRowSet(sql);
        return null;
    }

    @Override
    public String addNewIngredient(Ingredient ingredient) {
        String sql = "INSERT INTO ingredients() VALUES (DEFAULT,'"
                + ingredient.getName() + "' ,'" + ingredient.getInventory() + "')";
        jdbc.update(sql);
        return "redirect:/ingredients";
    }

    public Ingredient read(int id) {

        SqlRowSet ingredients = jdbc.queryForRowSet("SELECT * FROM ingredients where idIngredients ='" + id + "'");
        while (ingredients.next()) {

            return new Ingredient (ingredients.getInt("idIngredients"),
                    ingredients.getString("name"),
                    ingredients.getDouble("inventory"));
        }
        return read(id) ;
    }


    @Override
    public void Edit(Ingredient ingredients) {
        jdbc.update("UPDATE ingredients SET "

                + "name='" + ingredients.getName() +"' , "
                + "inventory='" + ingredients.getInventory()
                +"' WHERE idIngredients = '"+ ingredients.getId()+"'");
    }


    @Override
    public void delete(int id) {

        this.jdbc.update("DELETE FROM ingredients WHERE idIngredients='" + id +  "'");
        Long.valueOf(id);
    }



}






