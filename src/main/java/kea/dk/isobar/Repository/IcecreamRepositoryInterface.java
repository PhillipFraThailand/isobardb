package kea.dk.isobar.Repository;

import kea.dk.isobar.Model.Icecream;
import kea.dk.isobar.Model.Ingredient;

import java.util.List;

public interface IcecreamRepositoryInterface {

    List<Icecream> getIceCreams();
    void create(Icecream icecream);

    Icecream read(int id);

    void Edit (Icecream icecream);

    void delete(int id);


}

