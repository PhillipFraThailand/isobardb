package kea.dk.isobar.Repository;


import kea.dk.isobar.Model.Icecream;

import kea.dk.isobar.Model.IcecreamIngredients;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository

public class IcecreamIngredientsRepository implements IcecreamIngredientsRepositoryInterface {

    @Autowired
    JdbcTemplate jdbc;

    @Override
    public ArrayList<IcecreamIngredients> getIcecreamIngredients(Icecream iceCream) {
        SqlRowSet rows =
                jdbc.queryForRowSet("SELECT * FROM icecreamingredients where fkIcecream ='" + iceCream.getId() + "'");
        ArrayList<IcecreamIngredients> icecreamIngredients = new ArrayList<IcecreamIngredients>();
        while (rows.next()) {
            icecreamIngredients.add(
                    new IcecreamIngredients(rows.getInt("idicecreamingredients"),
                            rows.getInt("fkIcecream"),
                            rows.getInt("fkIngredients"),
                            rows.getDouble("Quantity"))
            );
        }

        return icecreamIngredients;
    }

    @Override
    public void produce(ArrayList<IcecreamIngredients> icecreamIngredients) {

        for (IcecreamIngredients ic : icecreamIngredients) {

            String sql = String.format("UPDATE ingredients SET inventory = inventory - %s" +
                    " WHERE idIngredients = %s", ic.getQuantity(), ic.getFkIngredients());
            System.out.print(sql);
            jdbc.update(sql);
            // UPDATE ingredients
            // SET inventory = inventory - ic.qty
            // WHERE idIngredient = ic.ingredientId
        }
    }
}


