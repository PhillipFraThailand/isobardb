package kea.dk.isobar.Repository;



import kea.dk.isobar.Model.Ingredient;

import java.sql.SQLException;
import java.util.List;

public interface IngredientRepositoryInterface {
    List<Ingredient> getIngredients();
    List<Ingredient> getIceCreamIngredients(int iceCreamId);
  //  String addNewIngredient(String name, double inventory) throws SQLException;
    String addNewIngredient(Ingredient ingredient) throws SQLException;

    Ingredient read(int id);

    void Edit (Ingredient ingredient);

    void delete(int id);

}
