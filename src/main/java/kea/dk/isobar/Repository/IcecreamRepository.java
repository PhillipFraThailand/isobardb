package kea.dk.isobar.Repository;

import kea.dk.isobar.Model.Icecream;
import kea.dk.isobar.Model.Ingredient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;


@Repository
public class IcecreamRepository implements IcecreamRepositoryInterface {

    @Autowired
    JdbcTemplate jdbc;
    SqlRowSet sqlRowSet;

    @Override
    public List<Icecream> getIceCreams() {
        List<Icecream> icecreams = new ArrayList<>();
        String sql = "SELECT * FROM icecreams";
        sqlRowSet = jdbc.queryForRowSet(sql);


        while (sqlRowSet.next()) {
            icecreams.add(new Icecream(sqlRowSet.getInt("idIcecreams"),
                    sqlRowSet.getString("name")
            ));

        }
        return icecreams;

    }

    @Override
    public void create(Icecream icecream) {

        jdbc.update("INSERT INTO icecreams(name) " +
                "VALUES('" + icecream.getName() + "')");

    }

    public Icecream read(int id) {

        SqlRowSet icecreams = jdbc.queryForRowSet("SELECT * FROM icecreams where idIcecreams ='" + id + "'");
        while (icecreams.next()) {

            return new Icecream (icecreams.getInt("idIcecreams"),
                                             icecreams.getString("name"));
        }
        return read(id) ;
    }


    @Override
    public void Edit(Icecream icecreams) {
        jdbc.update("UPDATE icecreams SET "

                + "name='" + icecreams.getName()
                +"' WHERE idIcecreams = '"+ icecreams.getId()+"'");
    }


    @Override
    public void delete(int id) {

        this.jdbc.update("DELETE FROM icecreams WHERE idIcecreams='" + id +  "'");
        Long.valueOf(id);
    }






}