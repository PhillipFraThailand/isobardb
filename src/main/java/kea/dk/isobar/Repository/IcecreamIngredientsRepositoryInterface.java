package kea.dk.isobar.Repository;

import kea.dk.isobar.Model.Icecream;
import kea.dk.isobar.Model.IcecreamIngredients;

import java.util.ArrayList;

public interface IcecreamIngredientsRepositoryInterface {

    ArrayList<IcecreamIngredients> getIcecreamIngredients(Icecream icecream);
    void produce(ArrayList<IcecreamIngredients> getIcecreamIngredients);
}
